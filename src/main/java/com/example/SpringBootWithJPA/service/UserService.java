package com.example.SpringBootWithJPA.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.example.SpringBootWithJPA.domain.EmpUser;
import com.example.SpringBootWithJPA.repository.UserRepository;

@Service
//@Scope(value="prototype")
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	public Optional<EmpUser> getUserById(Integer id) {
		return userRepository.findById(id);
	}
	
	public List<EmpUser> getUsers(){
		return userRepository.findAll();
	}
	
	public void setUser(EmpUser user) {
		userRepository.save(user);
	}
	
	public void deleteUser(int id) {
		userRepository.deleteById(id);
	}

}
