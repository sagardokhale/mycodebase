package com.example.SpringBootWithJPA.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.SpringBootWithJPA.domain.EmpUser;
import com.example.SpringBootWithJPA.service.UserService;

@RestController
//@Controller
@CrossOrigin(origins = "http://localhost:4200")
//@Component
@RequestMapping("/users")
public class UserConroller {
	
	@Autowired
	private UserService userService;

	@GetMapping("/{id}")
	public ResponseEntity<?> getUser(@PathVariable int id){
		return new ResponseEntity<>(userService.getUserById(id),HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<EmpUser>> getUsers(){
		return new ResponseEntity<>(userService.getUsers(),HttpStatus.OK);
	}
	
	/*@GetMapping
	public List<EmpUser> getUsers(){
		return userService.getUsers();
	}*/
	
	@PostMapping
	public void addUser(@RequestBody EmpUser user) {
		userService.setUser(user);
	}
	
	@DeleteMapping("/{id}")
	public void deleteUser(@PathVariable int id) {
		userService.deleteUser(id);
	}
	
	 @GetMapping("/greeting")
	    public ModelAndView greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
	        model.addAttribute("name", name);
	        return new ModelAndView("greeting");
	    }
}
