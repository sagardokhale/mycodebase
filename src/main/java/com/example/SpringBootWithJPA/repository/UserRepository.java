package com.example.SpringBootWithJPA.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.SpringBootWithJPA.domain.EmpUser;

@Repository
public interface UserRepository extends JpaRepository<EmpUser, Integer> {
	

}
