package com.example.SpringBootWithJPA.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity(name="EMPUSER")
public class EmpUser {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO,generator="SEQ")
	@SequenceGenerator(name = "SEQ", sequenceName = "EMP_SEQ")
	private int id;
	
	
	@Column
	private String name;
	
	@Column
	private int age;
	
	@Column
	private int salary;
	
	public EmpUser() {
		
	}
	
	public EmpUser(int id, String name, int age, int salary) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}

}
